import React, { Component } from "react";
import { data } from "../../data/dataGlasses";
import styles from "./changeGlassses.module.css";

export default class ChangeGlasses extends Component {
  state = {
    glasses: data,
    name: "",
    price: "",
    desc: "",
    url: "",
    display: "none",
  };

  handleChooseGlasses = (index) => {
    this.setState({
      display: "block",
      name: this.state.glasses[index].name,
      price: this.state.glasses[index].price,
      desc: this.state.glasses[index].desc,
      url: this.state.glasses[index].url,
    });
  };

  renderListGlass = () => {
    return this.state.glasses.map((glasses, index) => {
      return (
        <div className="col-4 mb-5">
          <img
            onClick={() => this.handleChooseGlasses(index)}
            style={{ width: "200px", cursor: "pointer" }}
            src={glasses.url}
            alt=""
          />
        </div>
      );
    });
  };

  render() {
    return (
      <div className="container text-center">
        <div className={styles.imgModel}>
          <img
            style={{ width: "300px" }}
            src="./glassesImage/model.jpg"
            alt=""
          />

          <img className={styles.glassesModel} src={this.state.url} alt="" />

          <div
            style={{ display: `${this.state.display}` }}
            className={styles.overlay}
          >
            <h3>{this.state.name}</h3>
            <span className="text-danger font-weight-bold">
              {this.state.price}
            </span>
            <p>{this.state.desc}</p>
          </div>
        </div>
        <div className="list__glasses row mt-4">{this.renderListGlass()}</div>
      </div>
    );
  }
}
